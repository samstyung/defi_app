# A project for creating defi app using truffle.


To compile:
```
truffle compile
```

To deploy smart contract to Ganache:
```
truffle migrate
```

To check balance of MyToken:
```
truffle exec scripts/getMyTokenBalance.js
```

To convert MyToken to FarmToken:
```
truffle exec scripts/convertMyTokenToFarmToken.js 
```

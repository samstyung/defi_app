const MyToken = artifacts.require('MyToken');
const FarmToken = artifacts.require('FarmToken');
module.exports = async function(callback) {
  const accounts = await new web3.eth.getAccounts();
  const myToken = await MyToken.deployed(); 
  const farmToken = await FarmToken.deployed(); 
  
  const allowanceBefore = await myToken.allowance(accounts[0], farmToken.address);
  console.log('MyToken Amount that FarmToken contract can transfer on our behalf: ' + allowanceBefore.toString());
  
  await myToken.approve(farmToken.address, web3.utils.toWei('100', 'ether'));
  const allowanceAfter = await myToken.allowance(accounts[0], farmToken.address);
  console.log('MyToken Amount that FarmToken contract can transfer on our behalf After Approval: ' + allowanceAfter.toString());
  
  balanceMyTokenAccounts0 = await myToken.balanceOf(accounts[0]);
  balanceMyTokenFarmToken = await myToken.balanceOf(farmToken.address);
  balanceFarmTokenAccounts0 = await farmToken.balanceOf(accounts[0]);
  balanceFarmTokenFarmToken = await farmToken.balanceOf(farmToken.address);

  console.log('')
  console.log('*** Balance ***')
  console.log('- accounts[0] -')
  console.log('MyToken: ' + web3.utils.fromWei(balanceMyTokenAccounts0.toString()))
  console.log('FarmToken: ' + web3.utils.fromWei(balanceFarmTokenAccounts0.toString()))
  console.log('- FarmToken Contract -')
  console.log('MyToken: ' + web3.utils.fromWei(balanceMyTokenFarmToken.toString()))
  console.log('FarmToken: ' + web3.utils.fromWei(balanceFarmTokenFarmToken.toString()))
  console.log('')
  
  console.log('Now - Call Deposit Function')
  await farmToken.deposit(web3.utils.toWei('100', 'ether'));
  
  balanceMyTokenAccounts0 = await myToken.balanceOf(accounts[0]);
  balanceMyTokenFarmToken = await myToken.balanceOf(farmToken.address);
  balanceFarmTokenAccounts0 = await farmToken.balanceOf(accounts[0]);
  balanceFarmTokenFarmToken = await farmToken.balanceOf(farmToken.address);

  console.log('')
  console.log('*** Balance ***')
  console.log('- accounts[0] -')
  console.log('MyToken: ' + web3.utils.fromWei(balanceMyTokenAccounts0.toString()))
  console.log('FarmToken: ' + web3.utils.fromWei(balanceFarmTokenAccounts0.toString()))
  console.log('- FarmToken Contract -')
  console.log('MyToken: ' + web3.utils.fromWei(balanceMyTokenFarmToken.toString()))
  console.log('FarmToken: ' + web3.utils.fromWei(balanceFarmTokenFarmToken.toString()))
  console.log('')


  callback();
}